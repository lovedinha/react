export class ContaCorrente {
    //Se quiser colocar um campo privado, basta adicionar # na frente 
    //Quando atributo for privado, ele n pode ser acessado fora da classe e nem visto em um console.log de fora da mesma
    agencia;
    //#saldo = 0;
    //No ex. abaixo, os devs aderiram o _ na frente do atributo apenas para controlar, fazendo com que seja identificado q
    //quando estiver com underline, o campo somente dever� ser atribuido pelos m�todos da classe.
    _saldo = 0;

    sacar(valor) {
        if (this._saldo >= valor) {
            this._saldo -= valor;
            return valor;
        }

    }

    depositar(valor) {
        if (valor < 0) return;

        this._saldo += valor;

    }
}
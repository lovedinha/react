import {Cliente} from "./Cliente.js"
import {ContaCorrente} from "./ContaCorrente.js"
const cliente1 = new Cliente();
const cliente2 = new Cliente();

cliente1.nome = "Rodrigo";
cliente1.cpf = 12345600078;

cliente2.nome = "Lidia";
cliente2.cpf = 11122354887;

const contaRodrigo = new ContaCorrente();
contaRodrigo.agencia = 1001;


contaRodrigo.depositar(100);
contaRodrigo.depositar(100);
contaRodrigo.depositar(100);
contaRodrigo.depositar(100);

const valorSacado = contaRodrigo.sacar(50);

console.log(valorSacado);
console.log(contaRodrigo);
